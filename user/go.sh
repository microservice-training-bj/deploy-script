#! /bin/sh -e
PORT=$1
PROFILE=$2
ssh $REMOTE_SERVER_USER@$REMOTE_SERVER_IP /bin/bash << EOF
  sudo docker run -d -name user -p ${PORT}:8080 -e PROFILE=${PROFILE} -e PROJECT_OPTS=${PROJECT_OPTS}  10.202.129.124:5000/user:0.1.${USER-pipeline_label}
EOF
